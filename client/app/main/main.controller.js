'use strict';

(function() {

class MainController {

  constructor($http, $scope) {
    this.$http = $http;
    this.$scope = $scope;
    $scope.dataError = false;
    $scope.$on("dataFail",function(event, data)
    {
      $scope.dataError = true;
    });
  }
}

angular.module('busStopServiceApp')
  .component('main', {
    templateUrl: 'app/main/main.html',
    controller: MainController
  });

})();
