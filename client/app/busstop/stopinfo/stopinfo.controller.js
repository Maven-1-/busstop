'use strict';

angular.module('busStopServiceApp')
  .controller('StopinfoCtrl', function ($scope, $timeout, uiGmapIsReady, BusStopDataService, Util, donatorinfo, $q) {

    $q(function(resolve, reject) {//this is just an empty call for the promises to catch the first thrown error in BusStopDataService.getAllStops.
      return resolve();
    })
    .then(BusStopDataService.getAllStops)
    .then(function(stops)
    {
      $scope.max = 700;
      $scope.busStops = stops;
      $scope.selectedBusStop = stops[Util.getRandomInt(0,stops.length)];
      return $scope.selectedBusStop;
    })
     .then(BusStopDataService.getDonationsForStop)
     .then(function(res)
     {
      $scope.donations = res;
      $scope.topDonations = Util._.sortBy($scope.donations, function(item){ return -item.amount;} ).slice(0,5);

      donatorinfo.setStopID($scope.selectedBusStop.stopId);
      $scope.map = { center: { latitude: parseFloat($scope.selectedBusStop.lat), longitude:parseFloat($scope.selectedBusStop.lng) }, zoom: 16 };
      $scope.markers = [];
      $scope.$watch($scope.active, function() {
        $timeout(function() {
          uiGmapIsReady.promise().then(function () {
            Util._.forEach($scope.busStops,function(item)
            {
              $scope.markers.push({
                idKey: item.stopId,
                coords: { latitude: parseFloat(item.lat), longitude:parseFloat(item.lng) }

              });
            })
          });
        }, 0);
      });
    }).catch(function(err)
    {
      console.error("ERR: " + err);
      //TODO: always a better idea to create an error service and create one error each time this happens
      $scope.$emit("dataFail",{err:err, msg:"An unexcepted problem occured. Please try and reload the page"});
    });

    $scope.onSelectionUpdate = function() {

      $scope.map = { center: { latitude: parseFloat($scope.selectedBusStop.lat), longitude:parseFloat($scope.selectedBusStop.lng) }, zoom: 17 };

      $q(function(resolve, reject) {
        return resolve($scope.selectedBusStop.stopId);
      }).then(BusStopDataService.getDonationsForStop)
      .then(function(donations)
      {
        $scope.donations = donations;
        $scope.topDonations = Util._.sortBy($scope.donations, function(item){ return -item.amount;} ).slice(0,5);
        console.dir($scope.donations);
        donatorinfo.setStopID($scope.selectedBusStop.stopId);
        $scope.dataError = false;
      }).catch(function(err)
      {
        $scope.dataError = true;
      });


    };

    $scope.$on("donate",function(event, args1)
    {
      $q(function(resolve, reject) {
        $scope.selectedBusStop.donationsRaisedInDollars += args1.amount;
        return resolve($scope.selectedBusStop.stopId);
      }).then(BusStopDataService.getDonationsForStop)
      .then(function(donations)
      {
        $scope.donations = donations;
        $scope.topDonations = Util._.sortBy($scope.donations, function(item){ return -item.amount;} ).slice(0,10);
        $scope.dataError = false;
      }).catch(function(err)
      {
        $scope.dataError = true;
      });

    });


  });
