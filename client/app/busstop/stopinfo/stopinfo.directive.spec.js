'use strict';

describe('Directive: stopinfo', function () {

  // load the directive's module and view
  beforeEach(module('busStopServiceApp'));
  beforeEach(module('app/busstop/stopinfo/stopinfo.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should have at least one of the elements visible', inject(function ($compile) {
    element = angular.element('<stopinfo></stopinfo>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toContain('Total Donation');
  }));
});
