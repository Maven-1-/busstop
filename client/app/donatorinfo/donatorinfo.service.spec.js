'use strict';

describe('Service: donatorinfo', function () {

  // load the service's module
  beforeEach(module('busStopServiceApp'));

  // instantiate service
  var donatorinfo;
  beforeEach(inject(function (_donatorinfo_) {
    donatorinfo = _donatorinfo_;
  }));

  describe('Donatorinfo property check', function(){
    it('should have properties set and get functions', function () {
      expect(!!donatorinfo).toBe(true);

      expect(donatorinfo.setDonatorInfo).toBeDefined();
      expect(donatorinfo.getDonatorInfo).toBeDefined();
      expect(donatorinfo.setStopID).toBeDefined();
      expect(donatorinfo.getStopID).toBeDefined();

    });

    it('should set and get donator info', function () {

      let name = "JohnDoe";
      let card = {
        number :"1101001101001001",
        cvc: "1144",
        month: '12',
        year: '18'
      };
      let email = "john@doe.com";
      let amount = 5;
      let expectedStopID = 1;

      donatorinfo.setStopID(expectedStopID);
      let actualStopID = donatorinfo.getStopID();
      expect(actualStopID).toEqual(expectedStopID);

      donatorinfo.setDonatorInfo(name, card, email, amount);
      let actualInfo = donatorinfo.getDonatorInfo();

      expect(actualInfo.name).toMatch(name);
      expect(actualInfo.card.number).toMatch(card.number);
      expect(actualInfo.card.cvc).toMatch(card.cvc);
      expect(actualInfo.card.month).toMatch(card.month);
      expect(actualInfo.card.year).toMatch(card.year);
      expect(actualInfo.email).toMatch(email);
      expect(actualInfo.amount).toEqual(amount);

    });


  })




});
