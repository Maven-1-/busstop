'use strict';

describe('Directive: error', function () {

  // load the directive's module and view
  beforeEach(module('busStopServiceApp'));
  beforeEach(module('app/error/error.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should contain element', inject(function ($compile) {
    element = angular.element('<error></error>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toContain('There was an error retrieving data. Please try reloading the page.');
  }));
});
