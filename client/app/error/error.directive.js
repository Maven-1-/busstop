'use strict';

angular.module('busStopServiceApp')
  .directive('error', function () {
    return {
      templateUrl: 'app/error/error.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
