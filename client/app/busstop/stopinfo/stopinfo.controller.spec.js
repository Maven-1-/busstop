'use strict';

describe('Controller: StopinfoCtrl', function () {

  // load the controller's module
  beforeEach(module('busStopServiceApp'));

  var StopinfoCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StopinfoCtrl = $controller('StopinfoCtrl', {
      $scope: scope
    });
  }));

  it('No tests implemented yet', function () {
    expect(1).toEqual(1);
  });
});
