'use strict';

angular.module('busStopServiceApp.donatorinfo',[])
  .service('donatorinfo', function () {
    var donator = {};
    var donationDest = 0;
    var setStopID = function(stopID){
      donationDest = stopID;
    };
    var setDonatorInfo = function(name, card, email, amount){
      donator.name = name;
      donator.card = card;
      donator.email = email;
      donator.amount = amount;
    };
    var getDonatorInfo = function(){
      return donator;
    };
    var getStopID = function(){
      return donationDest;
    };
    return {
      setDonatorInfo: setDonatorInfo,
      setStopID: setStopID,
      getStopID: getStopID,
      getDonatorInfo: getDonatorInfo
    };
  });
