'use strict';

describe('Directive: makedonation', function () {

  // load the directive's module and view
  beforeEach(module('busStopServiceApp'));
  beforeEach(module('app/makedonation/makedonation.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should have make a donation text inside', inject(function ($compile) {
    element = angular.element('<makedon></makedon>');
    element = $compile(element)(scope);
    scope.$apply();

    expect(element.text()).toContain('Make a donation');
  }));
});
