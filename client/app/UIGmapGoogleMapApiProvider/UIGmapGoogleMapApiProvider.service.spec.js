'use strict';

describe('Service: UIGmapGoogleMapApiProvider', function () {

  // load the service's module
  beforeEach(module('busStopServiceApp'));

  // instantiate service
  var UIGmapGoogleMapApiProvider;
  beforeEach(inject(function (_uiGmapIsReady_) {
    UIGmapGoogleMapApiProvider = _uiGmapIsReady_;
  }));

  it('should do something', function () {
    expect(!!UIGmapGoogleMapApiProvider).toBe(true);
  });

});
