'use strict';

angular.module('busStopServiceApp')
  .controller('makedonation', function ($rootScope, $scope, BusStopDataService, donatorinfo, Util, $q) {
    $scope.texts = {
      donateButton : "Donate"
    };
    $scope.isProcessing = false;
    $scope.donator =
    {
      card : {
        number: '',
        cvc: '',
        month: '',
        year: ''
      },
      name: '',
      email: ''
    };
    $scope.alerts =[];

    $scope.resetForm = function()
    {
      $scope.donator =
      {
        card : {
          number: '',
          cvc: '',
          month: '',
          year: ''
        },
        name: '',
        email: ''
      };
      $scope.myform.$setPristine();
    };
    $scope.paymentSuccessView = false;
    $scope.paymentFailView = false;
    $scope.closePaymentAlert = function(index)
    {
      console.log("closePaymentAlert");
      $scope.alerts.splice(index, 1);
    };
    $scope.slider = {
      value: 5,
      options: {
        floor: 5,
        ceil: 100,
        translate: function(value, sliderId, label) {
          switch (label) {
            case 'model':
              return '<b> Donate:</b> $' + value;
            case 'high':
              return '<b> Donate:</b> $' + value;
            default:
              return '$' + value
          }
        }
      }
    };
    $scope.formHasErrors = function () {

      if(!Util.isBlank($scope.donator.name) && Util.isBlank($scope.donator.email))
      {
        return true;
      }
      return (Util.isBlank($scope.donator.card.number)
        || Util.isBlank($scope.donator.card.cvc)
        || Util.isBlank($scope.donator.card.month)
        || Util.isBlank($scope.donator.card.year));

    };

    $scope.donate = function()
    {
      $scope.alerts = [];
      $scope.texts.donateButton = 'Processing...';
      $scope.isProcessing = true;
      //TODO: More validation and input sanitization required here
      let amount = $scope.slider.value;
      let name = ($scope.donator.name === "") ? "Anonymous": $scope.donator.name;
      let email = $scope.donator.email;
      // let cardNumber = $scope.donator.card.number;
      // let cvc = $scope.donator.card.cvc;
      // let month =$scope.donator.card.month;
      // let year = $scope.donator.card.year;

      donatorinfo.setDonatorInfo(name, $scope.donator.card, email, amount);

      console.log("name: " + $scope.donator.name);
      console.log("email: " + $scope.donator.email);
      console.dir($scope.donator.card);
      console.log("stopID" + donatorinfo.getStopID());
      $q(function(resolve, reject) {
        let donationData ={
          stopID: donatorinfo.getStopID(),
          amount:amount,
          name: name,
          email: email
        };
        return resolve(donationData);
      }).then(BusStopDataService.addDonation)
        .then(function(){
        $rootScope.$broadcast("donate", {amount : amount});
        $scope.resetForm();
        $scope.alerts.push({type:"success", msg: 'Payment Successful! Thank you.'});
      }).catch(function(err){
        console.log("donation error catch");
        $scope.alerts.push({type:"danger", msg:  'Oh snap! Something went wrong while processing your payment. Please try again!'});
      }).finally(function(){
          $scope.texts.donateButton = "Donate";
          $scope.isProcessing = false;
        })
    }
  });
