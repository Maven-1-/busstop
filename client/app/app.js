'use strict';

angular.module('busStopServiceApp', [
  'busStopServiceApp.constants',
  'busStopServiceApp.donatorinfo',
  'credit-cards',
  'uiGmapgoogle-maps',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'rzModule'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  }).config(function($provide) {
      $provide.decorator('$exceptionHandler', ['$delegate', function($delegate) {
        return function(exception, cause) {
          $delegate(exception, cause);
          //alert(exception.message);
          //Get some error logging to a remote service here.
        };
      }]);
});
