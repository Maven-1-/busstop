'use strict';

angular.module('busStopServiceApp')
  .directive('stopinfo', function () {
    return {
      templateUrl: 'app/busstop/stopinfo/stopinfo.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });
