'use strict';

describe('Service: BusStopDataService', function () {

  // load the service's module
  beforeEach(module('busStopServiceApp'));

  // instantiate service
  var BusStopDataService;
  beforeEach(inject(function (_BusStopDataService_) {
    BusStopDataService = _BusStopDataService_;
  }));

  it('should exist', function () {
    expect(!!BusStopDataService).toBe(true);
  });
  

});
