'use strict';

// angular.module('busStopServiceApp')
//   .provider('UIGmapGoogleMapApi', function UIGmapGoogleMapApiProvider() {
//
//     // Private constructor
//     function Greeter() {
//       this.greet = function () {
//         return salutation;
//       };
//     }
//
//     // Public API for configuration
//     this.configure = function (s) {
//       salutation = s;
//     };
//
//     // Method for instantiating
//     this.$get = function () {
//       return new Greeter();
//     };
//   });

angular.module('busStopServiceApp').config(['uiGmapGoogleMapApiProvider', function(GoogleMapApiProviders)
{
  GoogleMapApiProviders.configure({
    key: 'AIzaSyBuxva6smrj05SfyLChYGbBKBLaZfcM15M',
    v: '3.20', //defaults to latest 3.X anyhow
    libraries: 'weather,geometry,visualization'
  });
}]);
