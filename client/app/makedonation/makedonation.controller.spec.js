'use strict';

describe('Controller: MakedonationCtrl', function () {

  // load the controller's module
  beforeEach(module('busStopServiceApp'));

  var MakedonationCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MakedonationCtrl = $controller('makedonation', {
      $scope: scope
    });
  }));

  it('No tests implemented yet...', function () {
    expect(1).toEqual(1);
  });
});
