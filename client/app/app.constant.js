(function(angular, undefined) {
'use strict';

angular.module('busStopServiceApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);