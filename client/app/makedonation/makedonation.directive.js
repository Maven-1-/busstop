'use strict';

var myApp = angular.module('busStopServiceApp');

myApp.directive('makedon', function () {
    return {
      templateUrl: 'app/makedonation/makedonation.html',
      restrict: 'EA',
      link: function (scope, element, attrs) {
      }
    };
  });

//
// myApp.directive('validUsername', function () {
//   return {
//     require: 'ngModel',
//     link: function (scope, elm, attrs, ctrl) {
//       ctrl.$parsers.unshift(function (viewValue) {
//         // Any way to read the results of a "required" angular validator here?
//         var isBlank = viewValue === '';
//         var invalidChars = !isBlank && !/^[A-z0-9]+$/.test(viewValue);
//         var invalidLen = !isBlank && !invalidChars && (viewValue.length < 5 || viewValue.length > 20);
//         ctrl.$setValidity('isBlank', !isBlank);
//         ctrl.$setValidity('invalidChars', !invalidChars);
//         ctrl.$setValidity('invalidLen', !invalidLen);
//         scope.usernameGood = !isBlank && !invalidChars && !invalidLen;
//         //scope.donator.name = viewValue;
//
//       })
//     }
//   }
// });

